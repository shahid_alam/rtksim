/* ==========================================================================

	LOG.C		Task

---------------------------------------------------------------------------*/

// File = LOG.C

#include "all.h"								// include all headers



/* ===================================================

	log_event() function

PURPOSE:	This function logs event by sending a message
			to the LOG task, which writes the log to the
			log file, and increments the event counter.
SETUP:		*string = string to be logged (write to file).
CALL:		log_event()
RETURN:		None.

-----------------------------------------------------*/
void log_event(const char *string)
{
	task_msg_t	t_msg;

	++g.u_count_events;
	get_buffer(&g.t_buffer_list, &t_msg.pac_data);	// get buffer, store pointer
	strcpy(t_msg.pac_data, string);
	t_msg.c_task = TN_LOG;							// store display task number
	t_msg.u_opcode = OP_LOG_EVENT;					// store op code to display data
	add_to_bottom(&g.t_task_list, (char*)&t_msg);	// schedule the task
}

/* ===================================================

	time_stamp() function

PURPOSE:	This function gets the timestamping and stores in the
			given string.
SETUP:		*ac_timestamp = pointer to string variable.
CALL:		time_stamp(ac_timestamp);
RETURN:		None.

-----------------------------------------------------*/
void time_stamp(char *pac_timestamp)
{
	struct date		d;							// temporary date structure
	struct time		t;							// temporary time structure

	getdate(&d);								// get date into d
	gettime(&t);								// get time into t
	sprintf(pac_timestamp, "%04d/%02d/%02d %02d:%02d:%02d ",
		d.da_year, d.da_mon, d.da_day,
		t.ti_hour, t.ti_min, t.ti_sec);
}


/* ===================================================

	log_task() Task

PURPOSE:	This task performs the timestamping and logging.
SETUP:		None.
CALL:		log_task();
RETURN:		None.

-----------------------------------------------------*/
void log_task(task_msg_t t_msg)
{
	static FILE	*file_activity;
	static FILE	*file_event;
	static char activity_filename[80];
	static char	event_filename[80];
	char		ac_timestamp[80];							// local variable
	char		*p;											// temporary pointer to char
	char		str[256];

	p = t_msg.pac_data;										// store address in temporary pointer
	switch(t_msg.u_opcode)									// decode op code
	{
		case OP_LOG_INIT:									// initialize log
			g.c_activity_on = 0;
			g.c_event_on = 0;
			g.u_count_activity = 0;
			g.u_count_events = 0;
			break;
		case OP_LOG_ACTIVITY:								// log activity
			if (g.c_activity_on)							// if logging is on
			{
				fwrite(p, strlen(p), 1, file_activity);
				sprintf(t_msg.pac_data, " File = '%s'\n Samples = %d", 
					activity_filename, g.u_count_activity);	// add message
				t_msg.c_task = TN_DSP;						// store display task number
				t_msg.u_opcode = OP_DSP_ACTIVITY_LOG;		// store op code to display data
				add_to_bottom(&g.t_task_list, (char*)&t_msg);// schedule the task
			}
			else
				free_buffer(&g.t_buffer_list, &p);	// free the buffer
			break;

		case OP_LOG_OPEN_ACTIVITY:							// open activity log
			if((file_activity=fopen(p, "wb")) == NULL)
			{
				sprintf(t_msg.pac_data, "ERROR: Opening activity file %s", p);	// add message
				t_msg.c_task = TN_DSP;						// store display task number
				t_msg.u_opcode = OP_DSP_INPUT;				// store op code to display data
				add_to_bottom(&g.t_task_list, (char*)&t_msg);// schedule the task
				g.t_input_timer.u_count = 3*SECOND;			// start timer to clear input later
			}
			else
			{
				g.c_activity_on = 1;						// turn activity on
				strcpy(activity_filename, p);
				time_stamp(p);								// create timestamp
				strcat(p, "*** ACTIVITY LOG FILE OPENED ***\n");// append message
				strcat(p, "sample,c_max,c_main,c_idle,act_max,freq,activity,tasks,buffers\n");
				fwrite(p, strlen(p), 1, file_activity);		// write string to file
				sprintf(t_msg.pac_data, " File = '%s'\n Samples = %d", 
					activity_filename, g.u_count_activity);	// add message
				t_msg.c_task = TN_DSP;						// store display task number
				t_msg.u_opcode = OP_DSP_ACTIVITY_LOG;		// store op code to display data
				add_to_bottom(&g.t_task_list, (char*)&t_msg);// schedule the task
			}
			break;

		case OP_LOG_CLOSE_ACTIVITY:							// close activity log
			g.c_activity_on = 0;							// turn activity off
			g.u_count_activity = 0;
			time_stamp(str);								// create timestamp
			strcat(str, "*** ACTIVITY LOG FILE CLOSED ***\n");// append message
			fwrite(str, strlen(str), 1, file_activity);		// write string to file
			fclose(file_activity);
			t_msg.c_task = TN_DSP;							// store display task number
			t_msg.u_opcode = OP_DSP_CLR_ACTIVITY_LOG;		// store op code to display data
			add_to_bottom(&g.t_task_list, (char*)&t_msg);	// schedule the task
			break;

		case OP_LOG_EVENT:									// log event
			if (g.c_event_on)								// if logging is on
			{
				fwrite(p, strlen(p), 1, file_event);
				sprintf(t_msg.pac_data, " File = '%s'\n Events = %d", 
					event_filename, g.u_count_events);		// add message
				t_msg.c_task = TN_DSP;						// store display task number
				t_msg.u_opcode = OP_DSP_EVENT_LOG;			// store op code to display data
				add_to_bottom(&g.t_task_list, (char*)&t_msg);// schedule the task
			}
                        else
				free_buffer(&g.t_buffer_list, &p);	// free the buffer
			break;

		case OP_LOG_OPEN_EVENT:								// open event log
			if((file_event=fopen(p, "wb")) == NULL)
			{
				sprintf(t_msg.pac_data, "ERROR: Opening event file %s", p);	// add message
				t_msg.c_task = TN_DSP;						// store display task number
				t_msg.u_opcode = OP_DSP_INPUT;				// store op code to display data
				add_to_bottom(&g.t_task_list, (char*)&t_msg);// schedule the task
				g.t_input_timer.u_count = 3*SECOND;			// start timer to clear input later
			}
			else
			{
				g.c_event_on = 1;							// turn event on
				strcpy(event_filename, p);
				time_stamp(p);								// create timestamp
				strcat(p, "*** EVENT LOG FILE OPENED ***\n");// append message
				fwrite(p, strlen(p), 1, file_event);		// write string to file
				sprintf(t_msg.pac_data, " File = '%s'\n Events = %d", 
					event_filename, g.u_count_events);		// add message
				t_msg.c_task = TN_DSP;						// store display task number
				t_msg.u_opcode = OP_DSP_EVENT_LOG;			// store op code to display data
				add_to_bottom(&g.t_task_list, (char*)&t_msg);// schedule the task
			}
			break;

		case OP_LOG_CLOSE_EVENT:							// close event log
			g.c_event_on = 0;								// turn event off
			g.u_count_events = 0;
			time_stamp(str);								// create timestamp
			strcat(str, "*** EVENT LOG FILE CLOSED ***\n");	// append message
			fwrite(str, strlen(str), 1, file_event);		// write string to file
			fclose(file_event);
			t_msg.c_task = TN_DSP;							// store display task number
			t_msg.u_opcode = OP_DSP_CLR_EVENT_LOG;			// store op code to display data
			add_to_bottom(&g.t_task_list, (char*)&t_msg);	// schedule the task
			break;

		case OP_LOG_EXIT:									// exit log task
			fcloseall();									// close all files
			break;
	}
}

