/* ==========================================================================

	SCREEN.C		Library

	These functions provide box drawing functions for the display screen.

---------------------------------------------------------------------------*/

// File = SCREEN.C

#include "all.h"


// ----------------- Graphics Character Local Definitions -------------------

#define		CH_BOX_LT		218					// left top symbol
#define		CH_BOX_RT		191					// right top symbol
#define		CH_BOX_LB		192					// left bottom symbol
#define		CH_BOX_RB		217					// right bottom symbol
#define		CH_BOX_H		196					// horizontal symbol
#define		CH_BOX_V		179					// vertical symbol



/* ==========================================================================

	draw_box() function

PURPOSE:	This function draws a box on the screen with a border & title.
SETUP:		x0 = x-screen coordinate of left top corner,
			y0 = y-screen coordinate of left top corner,
			x  = size of box horizontally (x0 + x - 1 = right top),
			y  = size of box vertically (y0 + y - 1 = left bottom),
			*s = pointer to the title to be drawn on the box.
CALL:		draw_box(XY_ABCD_X0, XY_ABCD_Y0, XY_ABCD_X, XY_ABCD_Y, "Box Title");
RETURN:		None.

---------------------------------------------------------------------------*/
void draw_box(unsigned x0, unsigned y0, unsigned x, unsigned y, char *s)
{
	int		i;									// loop counter

	gotoxy(x0, y0);								// go to left top corner
	putch(CH_BOX_LT);							// draw left-top symbol
	for (i = x-2; i; --i)						// for top horizontal size
	{
		putch(CH_BOX_H);						// draw horizontal line symbol
	}
	putch(CH_BOX_RT);							// draw right-top symbol
	gotoxy(x0, y0 + y - 1);						// go to left bottom corner
	putch(CH_BOX_LB);							// draw left-bottom symbol
	for (i = x-2; i; --i)						// for top horizontal size
	{
		putch(CH_BOX_H);						// draw horizontal line symbol
	}
	putch(CH_BOX_RB);							// draw right-bottom symbol
	for (i = 0; i < (y-2); ++i)					// for vertical size
	{
		gotoxy(x0, y0 + i + 1);   				// go to left side
		putch(CH_BOX_V);						// draw vertical line symbol
		gotoxy(x0 + x - 1, y0 + i + 1);	        // go to right side
		putch(CH_BOX_V);						// draw vertical line symbol
	}
	gotoxy(x0 + x/2 - strlen(s)/2 - 1, y0);		// go to middle top line
	if (strlen(s))								// if string exists
	{
		putch(' ');								// draw leading space
		cputs(s);								// draw the top title string
		putch(' ');								// draw trailing space
	}
}


/* ==========================================================================

	clear_box() function

PURPOSE:	This function clears the inside of the box on the screen.
SETUP:		x0 = x-screen coordinate of left top corner,
			y0 = y-screen coordinate of left top corner,
			x  = size of box horizontally (x0 + x - 1 = right top),
			y  = size of box vertically (y0 + y - 1 = left bottom).
CALL:		clear_box(XY_ABCD_X0, XY_ABCD_Y0, XY_ABCD_X, XY_ABCD_Y);
RETURN:		None.

---------------------------------------------------------------------------*/
void clear_box(unsigned x0, unsigned y0, unsigned x, unsigned y)
{
	int		i, j;								// loop counter

	for (j = 0; j < y-2; ++j)					// for all lines (y axis)
	{
		gotoxy(x0+1, y0+j+1);					// go to beginning of new line
		for (i = x-2; i; --i)					// for all characters (x axis)
		{
			putch(' ');							// draw a blank character
		}
	}
}


/* ==========================================================================

	write_box() function

PURPOSE:	This function clears the inside of the box on the screen.
SETUP:		x0 = x-screen coordinate of left top corner,
			y0 = y-screen coordinate of left top corner,
			x  = size of box horizontally (x0 + x - 1 = right top),
			y  = size of box vertically (y0 + y - 1 = left bottom),
			*s = pointer to the string to be drawn inside the box.
CALL:		write_box(XY_ABCD_X0, XY_ABCD_Y0, XY_ABCD_X, XY_ABCD_Y, "Message");
RETURN:		None.

---------------------------------------------------------------------------*/
void write_box(unsigned x0, unsigned y0, unsigned x, unsigned y, char *s)
{
	int		i, j;								// loop counter

	for (j = 0; (j < y-2) && *s; ++j)			// for all lines and valid string
	{
		gotoxy(x0+1, y0+j+1);					// go to beginning of new line
		for (i = x-2; i && *s && (*s != '\n'); --i)	// for all characters and valid string
		{
			putch(*(s++));						// draw a character from the string
		}
		while ((*s < ' ') && *s)				// skip to end of line or string
		{
			s++;								// advance pointer
		}
	}
}


